// ForgottenGods

#pragma once
#include "C_GameFunctionLibrary.h"

#include "CoreMinimal.h"
#include "GameFramework/GameMode.h"
#include "C_CISRGameMode.generated.h"

UCLASS()
class CISR_API AC_CISRGameMode : public AGameMode
{
	GENERATED_BODY()
	
	public:
    AC_CISRGameMode();

    FOnGameStateChangedSignature OnGameStateChanged;

    virtual void StartPlay() override;
    virtual UClass* GetDefaultPawnClassForController_Implementation(AController* InController) override;
    virtual bool SetPause(APlayerController* PC, FCanUnpause CanUnpauseDelegate = FCanUnpause()) override;
    virtual bool ClearPause() override;
    UFUNCTION(BlueprintCallable)
    void Win();
    UFUNCTION(BlueprintCallable)
    void Loose();
    UFUNCTION(BlueprintCallable)
    void History();
    UFUNCTION(BlueprintCallable)
    void Game();

private:
    EGameState GameState = EGameState::Game;
    void SetGameState(EGameState State);
};
