// ForgottenGods

#pragma once
#include "C_GameFunctionLibrary.h"

#include "CoreMinimal.h"
#include "GameFramework/GameMode.h"
#include "C_MenuGameMode.generated.h"

UCLASS()
class CISR_API AC_MenuGameMode : public AGameMode
{
	GENERATED_BODY()

public:
    AC_MenuGameMode();

    virtual void StartPlay() override;
};
