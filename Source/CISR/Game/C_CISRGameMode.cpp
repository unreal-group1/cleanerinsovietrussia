// ForgottenGods

#include "C_CISRGameMode.h"
#include "C_GameHUD.h"
#include "C_CISRPlayerController.h"
#include "C_CISRCharacter.h"

AC_CISRGameMode::AC_CISRGameMode()
{
    PlayerControllerClass = AC_CISRPlayerController::StaticClass();
    DefaultPawnClass = AC_CISRCharacter::StaticClass();
    HUDClass = AC_GameHUD::StaticClass();
}

void AC_CISRGameMode::StartPlay()
{
    Super::StartPlay();

    History();
}

UClass* AC_CISRGameMode::GetDefaultPawnClassForController_Implementation(AController* InController)
{
    return Super::GetDefaultPawnClassForController_Implementation(InController);
}

bool AC_CISRGameMode::SetPause(APlayerController* PC, FCanUnpause CanUnpauseDelegate)
{
    const auto PauseSet = Super::SetPause(PC, CanUnpauseDelegate);
    if (PauseSet)
    {
        SetGameState(EGameState::Pause);
    }

    return PauseSet;
}

bool AC_CISRGameMode::ClearPause()
{
    const auto Unpaused = Super::ClearPause();
    if (Unpaused)
    {
        SetGameState(EGameState::Game);
    }

    return Unpaused;
}

void AC_CISRGameMode::Win()
{
    UE_LOG(LogTemp, Warning, TEXT("========== WIN =========="));
    SetGameState(EGameState::Win);
}

void AC_CISRGameMode::Loose()
{
    UE_LOG(LogTemp, Warning, TEXT("========== LOOSE =========="));
    SetGameState(EGameState::Loose);
}

void AC_CISRGameMode::History()
{
    UE_LOG(LogTemp, Warning, TEXT("========== HISTORY =========="));
    SetGameState(EGameState::History);
}

void AC_CISRGameMode::Game()
{
    UE_LOG(LogTemp, Warning, TEXT("========== GAME =========="));
    SetGameState(EGameState::Game);
}

void AC_CISRGameMode::SetGameState(EGameState State)
{
    if (GameState == State)
    {
        return;
    }

    GameState = State;
    OnGameStateChanged.Broadcast(GameState);
}
