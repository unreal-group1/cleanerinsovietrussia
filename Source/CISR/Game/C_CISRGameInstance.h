// ForgottenGods

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"

#include "C_GameFunctionLibrary.h"
#include "C_CISRGameInstance.generated.h"

 
UCLASS()
class CISR_API UC_CISRGameInstance : public UGameInstance
{
	GENERATED_BODY()
	
	public:
    FLevelData GetStartupLevel() const;
    void SetStartupLevel(const FLevelData& StartupLevelData);
    TArray<FLevelData> GetLevelsData() const;
    FName GetMenuLevelName() const;
    void SetSkipHistory();
    bool GetSkipHistory();
    
    protected:
    UPROPERTY(EditDefaultsOnly, Category = "Game")
    TArray<FLevelData> LevelsData;

    UPROPERTY(EditDefaultsOnly, Category = "Game", meta = (ToolTip = "Lavel names must be unique!"))
    FName MenuLevelName = NAME_None;

    private:
    FLevelData StartupLevel;
    bool bSkipHistory = false;
};
