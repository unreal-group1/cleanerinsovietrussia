// Fill out your copyright notice in the Description page of Project Settings.

using UnrealBuildTool;

public class CISR : ModuleRules
{
	public CISR(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;
	
		PublicDependencyModuleNames.AddRange(new string[] { 
			"Core", 
			"CoreUObject", 
			"Engine", 
			"InputCore",
            "EnhancedInput" });

        PublicIncludePaths.AddRange(new string[] {
            "CISR",
            "CISR/FunctionLibraries",
			"CISR/UI",
            "CISR/UI/Menu",
            "CISR/Game",
            "CISR/Player" });
    }
}
