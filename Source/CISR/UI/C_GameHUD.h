#pragma once

#include "C_GameFunctionLibrary.h"

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "C_GameHUD.generated.h"

class UC_BaseWidget;

UCLASS()
class CISR_API AC_GameHUD : public AHUD
{
	GENERATED_BODY()
	
	public:
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI")
    TSubclassOf<UC_BaseWidget> HistoryWidgetClass;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI")
    TSubclassOf<UC_BaseWidget> PauseWidgetClass;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI")
    TSubclassOf<UC_BaseWidget> LooseWidgetClass;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI")
    TSubclassOf<UC_BaseWidget> WinWidgetClass;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI")
    TSubclassOf<UC_BaseWidget> GameWidgetClass;

    virtual void BeginPlay() override;

    private:
    UPROPERTY()
    TMap<EGameState, UC_BaseWidget*> GameWidgets;
    UPROPERTY()
    UC_BaseWidget* CurrentWidget = nullptr;

    void OnGameStateChanged(EGameState State);
    void HideCurrentWidget();
    void ShowStateWidget(EGameState State);
};
