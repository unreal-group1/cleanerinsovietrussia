// ForgottenGods

#include "C_PauseWidget.h"
#include "C_CISRGameInstance.h"

#include "GameFramework/GameModeBase.h"
#include "Kismet/GameplayStatics.h"
#include "Components/Button.h"

void UC_PauseWidget::NativeOnInitialized()
{
    Super::NativeOnInitialized();

    if (!UnpauseButton)
    {
        return;
    }
    UnpauseButton->OnClicked.AddDynamic(this, &UC_PauseWidget::Unpause);
    MenuButton->OnClicked.AddDynamic(this, &UC_PauseWidget::Menu);
}

void UC_PauseWidget::OnAnimationFinished_Implementation(const UWidgetAnimation* Animation)
{
    if (Animation != HideAnimation || !GetWorld())
    {
        return;
    }
    const auto GameInstatnce = GetWorld()->GetGameInstance<UC_CISRGameInstance>();
    if (!GameInstatnce)
    {
        return;
    }
    const auto LevelName = GameInstatnce->GetStartupLevel().LevelName;
    UGameplayStatics::OpenLevel(this, LevelName);
}

void UC_PauseWidget::Menu()
{
    if (!GetWorld())
    {
        return;
    }
    const auto STUGameInstatnce = GetWorld()->GetGameInstance<UC_CISRGameInstance>();
    if (!STUGameInstatnce)
    {
        return;
    }
    if (STUGameInstatnce->GetMenuLevelName().IsNone())
    {
        UE_LOG(LogTemp, Error, TEXT("Menu level name is NONE"));
        return;
    }

    UGameplayStatics::OpenLevel(this, STUGameInstatnce->GetMenuLevelName());
}

void UC_PauseWidget::Unpause()
{
    if (!GetWorld() 
        || !GetWorld()->GetAuthGameMode())
    {
        return;
    }
    GetWorld()->GetAuthGameMode()->ClearPause();
}
