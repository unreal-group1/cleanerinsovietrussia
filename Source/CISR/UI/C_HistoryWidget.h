// ForgottenGods

#pragma once

#include "CoreMinimal.h"

#include "C_GameFunctionLibrary.h"
#include "C_BaseWidget.h"
#include "C_HistoryWidget.generated.h"

class UImage;
class UButton;

UCLASS()
class CISR_API UC_HistoryWidget : public UC_BaseWidget
{
	GENERATED_BODY()

public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI")
    TArray<FImageAndSound> HistoryImagesAndSounds;

    UPROPERTY(meta = (BindWidget))
    UImage* HistoryImage;

    UPROPERTY(meta = (BindWidget))
    UButton* NextImageButton;

    UPROPERTY(meta = (BindWidgetAnim), Transient)
    UWidgetAnimation* HideAnimation;

    virtual void NativeOnInitialized() override;
    virtual void OnAnimationFinished_Implementation(const UWidgetAnimation* Animation);

private:
    int8 CurrentIndexImageAndSound = 0;

    UFUNCTION()
    void OnNextImageGame();
};
