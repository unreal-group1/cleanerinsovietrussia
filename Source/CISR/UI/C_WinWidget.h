// ForgottenGods

#pragma once

#include "CoreMinimal.h"

#include "C_GameFunctionLibrary.h"
#include "C_BaseWidget.h"
#include "C_WinWidget.generated.h"

class UButton;

UCLASS()
class CISR_API UC_WinWidget : public UC_BaseWidget
{
	GENERATED_BODY()

    public:
    UPROPERTY(meta = (BindWidget))
    UButton* StartGameButton;

    UPROPERTY(meta = (BindWidget))
    UButton* MenuGameButton;

    UPROPERTY(meta = (BindWidgetAnim), Transient)
    UWidgetAnimation* HideAnimation;

    virtual void NativeOnInitialized() override;
    virtual void OnAnimationFinished_Implementation(const UWidgetAnimation* Animation);

    private:
    UFUNCTION()
    void OnStartGame();

    UFUNCTION()
    void OnMenuGame();
};
