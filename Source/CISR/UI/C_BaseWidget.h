// ForgottenGods

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "C_BaseWidget.generated.h"

UCLASS()
class CISR_API UC_BaseWidget : public UUserWidget
{
	GENERATED_BODY()
	
	public:
    virtual void NativeOnInitialized() override;
    void Show();

    protected:
    UPROPERTY(meta = (BindWidgetAnim), Transient)
    UWidgetAnimation* ShowAnimation;
    // Audio
    protected:
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Audio)
    USoundBase* SpeechSound;

    UFUNCTION()
    void PlaySpeechSound();
    UFUNCTION()
    void PlaySoundForWidget(USoundBase* Sound);

    void NativeVisibilityChanged(ESlateVisibility SlateVisibility);

    private:
    UAudioComponent* AudioComponent;
};
