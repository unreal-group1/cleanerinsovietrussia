// ForgottenGods

#include "C_BaseWidget.h"
#include "C_CISRGameInstance.h"
#include "C_CISRPlayerController.h"

#include "Components/AudioComponent.h"
#include "GameFramework/GameModeBase.h"

void UC_BaseWidget::NativeOnInitialized()
{
    if (!GetWorld() 
        || !GetWorld()->GetFirstPlayerController() 
        || !GetWorld()->GetFirstPlayerController()->GetCharacter())
    {
        return;
    }

    const auto PlayerController = Cast<AC_CISRPlayerController>(GetWorld()->GetFirstPlayerController());
    if (PlayerController)
    {
        AudioComponent = Cast<UAudioComponent>(PlayerController->GetComponentByClass(UAudioComponent::StaticClass()));
    }

    if (AudioComponent)
    {
        AudioComponent->bIsUISound = true;
        AudioComponent->bAutoActivate = false;
    }
    OnNativeVisibilityChanged.AddUObject(this, &UC_BaseWidget::NativeVisibilityChanged);
}

void UC_BaseWidget::Show()
{
    PlayAnimation(ShowAnimation);
}

void UC_BaseWidget::PlaySpeechSound()
{
    if (SpeechSound 
        && AudioComponent)
    {
        AudioComponent->SetSound(SpeechSound);
        AudioComponent->Play();
    }
}

void UC_BaseWidget::PlaySoundForWidget(USoundBase* Sound)
{
    if (AudioComponent)
    {
        AudioComponent->SetSound(Sound);
        AudioComponent->Play();
    }
}

void UC_BaseWidget::NativeVisibilityChanged(ESlateVisibility SlateVisibility)
{
    if (SlateVisibility == ESlateVisibility::Visible)
    {
        PlaySpeechSound();
    }
}
