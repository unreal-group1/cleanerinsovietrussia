// ForgottenGods

#include "C_WinWidget.h"
#include "C_CISRGameInstance.h"

#include "Components/Button.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetSystemLibrary.h"

void UC_WinWidget::NativeOnInitialized()
{
    Super::NativeOnInitialized();

    if (StartGameButton)
    {
        StartGameButton->OnClicked.AddDynamic(this, &UC_WinWidget::OnStartGame);
    }

    if (MenuGameButton)
    {
        MenuGameButton->OnClicked.AddDynamic(this, &UC_WinWidget::OnMenuGame);
    }
    PlaySpeechSound();
}

void UC_WinWidget::OnAnimationFinished_Implementation(const UWidgetAnimation* Animation)
{
    if (Animation != HideAnimation || !GetWorld())
    {
        return;
    }
    const auto GameInstatnce = GetWorld()->GetGameInstance<UC_CISRGameInstance>();
    if (!GameInstatnce)
    {
        return;
    }
    const auto LevelName = GameInstatnce->GetStartupLevel().LevelName;
    UGameplayStatics::OpenLevel(this, LevelName);
}

void UC_WinWidget::OnStartGame()
{
    PlayAnimation(HideAnimation);
}

void UC_WinWidget::OnMenuGame()
{
    if (!GetWorld())
    {
        return;
    }
    const auto STUGameInstatnce = GetWorld()->GetGameInstance<UC_CISRGameInstance>();
    if (!STUGameInstatnce)
    {
        return;
    }
    if (STUGameInstatnce->GetMenuLevelName().IsNone())
    {
        UE_LOG(LogTemp, Error, TEXT("Menu level name is NONE"));
        return;
    }

    UGameplayStatics::OpenLevel(this, STUGameInstatnce->GetMenuLevelName());
}
