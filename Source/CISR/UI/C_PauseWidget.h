// ForgottenGods

#pragma once

#include "CoreMinimal.h"
#include "C_BaseWidget.h"
#include "C_PauseWidget.generated.h"

class UButton;

UCLASS()
class CISR_API UC_PauseWidget : public UC_BaseWidget
{
    GENERATED_BODY()

    protected:
    UPROPERTY(meta = (BindWidget))
    UButton* MenuButton;
    UPROPERTY(meta = (BindWidget))
    UButton* UnpauseButton;

    UPROPERTY(meta = (BindWidgetAnim), Transient)
    UWidgetAnimation* HideAnimation;

    virtual void NativeOnInitialized() override;
    virtual void OnAnimationFinished_Implementation(const UWidgetAnimation* Animation);

    private:
    UFUNCTION()
    void Menu();
    UFUNCTION()
    void Unpause();
};
