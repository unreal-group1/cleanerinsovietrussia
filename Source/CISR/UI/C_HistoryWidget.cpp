// ForgottenGods

#include "C_HistoryWidget.h"
#include "C_CISRGameMode.h"

#include "Components/Button.h"
#include "Kismet/GameplayStatics.h"

void UC_HistoryWidget::NativeOnInitialized()
{
    Super::NativeOnInitialized();

    if (NextImageButton)
    {
        NextImageButton->OnClicked.AddDynamic(this, &UC_HistoryWidget::OnNextImageGame);
    }

    PlaySpeechSound();
}

void UC_HistoryWidget::OnAnimationFinished_Implementation(const UWidgetAnimation* Animation)
{
    if (Animation != HideAnimation || !GetWorld())
    {
        return;
    }
    auto GameMode = Cast<AC_CISRGameMode>(GetWorld()->GetAuthGameMode());
    GameMode->Game();
}

void UC_HistoryWidget::OnNextImageGame()
{
    if (HistoryImagesAndSounds.Num() >= CurrentIndexImageAndSound)
    {
        CurrentIndexImageAndSound = 0;
        PlayAnimation(HideAnimation);
        return;
    }

    PlaySoundForWidget(HistoryImagesAndSounds[CurrentIndexImageAndSound].Sound);
    HistoryImage = HistoryImagesAndSounds[CurrentIndexImageAndSound].Image;

    CurrentIndexImageAndSound++;
}
