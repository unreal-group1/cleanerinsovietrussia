// ForgottenGods

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "C_MenuHUD.generated.h"

UCLASS()
class CISR_API AC_MenuHUD : public AHUD
{
	GENERATED_BODY()
	
	protected:
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI")
    TSubclassOf<UUserWidget> MenuWidgetClass;

    virtual void BeginPlay() override;
};
