// ForgottenGods

#pragma once

#include "CoreMinimal.h"

#include "C_GameFunctionLibrary.h"
#include "C_BaseWidget.h"
#include "C_MenuWidget.generated.h"

class UButton;

UCLASS()
class CISR_API UC_MenuWidget : public UC_BaseWidget
{
	GENERATED_BODY()

	protected:
    UPROPERTY(meta = (BindWidget))
    UButton* StartGameButton;

    UPROPERTY(meta = (BindWidget))
    UButton* QuitGameButton;

    UPROPERTY(meta = (BindWidgetAnim), Transient)
    UWidgetAnimation* HideAnimation;

    virtual void NativeOnInitialized() override;
    virtual void OnAnimationFinished_Implementation(const UWidgetAnimation* Animation);

private:
    UFUNCTION()
    void OnStartGame();

    UFUNCTION()
    void OnQuitGame();
};
