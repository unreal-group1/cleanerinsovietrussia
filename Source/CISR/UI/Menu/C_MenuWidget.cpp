// ForgottenGods

#include "C_MenuWidget.h"
#include "C_CISRGameInstance.h"

#include "Components/Button.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetSystemLibrary.h"

void UC_MenuWidget::NativeOnInitialized()
{
    Super::NativeOnInitialized();

    if (StartGameButton)
    {
        StartGameButton->OnClicked.AddDynamic(this, &UC_MenuWidget::OnStartGame);
    }

    if (QuitGameButton)
    {
        QuitGameButton->OnClicked.AddDynamic(this, &UC_MenuWidget::OnQuitGame);
    }
    PlaySpeechSound();
}

void UC_MenuWidget::OnAnimationFinished_Implementation(const UWidgetAnimation* Animation)
{
    if (Animation != HideAnimation 
        || !GetWorld())
    {
        return;
    }
    const auto GameInstatnce = GetWorld()->GetGameInstance<UC_CISRGameInstance>();
    if (!GameInstatnce)
    {
        return;
    }
    const auto LevelName = GameInstatnce->GetStartupLevel().LevelName;
    UGameplayStatics::OpenLevel(this, LevelName);
}

void UC_MenuWidget::OnStartGame()
{
    PlayAnimation(HideAnimation);
}

void UC_MenuWidget::OnQuitGame()
{
    UKismetSystemLibrary::QuitGame(this, GetOwningPlayer(), EQuitPreference::Quit, true);
}
