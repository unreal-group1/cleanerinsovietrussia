// ForgottenGods

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "Sound/SoundBase.h"
#include "Components/Image.h"
#include "C_GameFunctionLibrary.generated.h"

// Game
UENUM(BlueprintType)
enum class EGameState : uint8
{
    StartLevel = 0,
    History,
    Pause,
    Win,
    Loose,
    Game
};

DECLARE_MULTICAST_DELEGATE_OneParam(FOnGameStateChangedSignature, EGameState);

// Level
USTRUCT(BlueprintType)
struct FLevelData
{
    GENERATED_USTRUCT_BODY()

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Game")
    FName LevelName = NAME_None;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Game")
    FName LevelDisplayName = NAME_None;
};

//History ImageAndSound
USTRUCT(BlueprintType)
struct FImageAndSound
{
    GENERATED_USTRUCT_BODY()

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UI")
    UImage* Image;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UI")
    USoundBase* Sound;
};

DECLARE_MULTICAST_DELEGATE_OneParam(FOnLevelSelectedSignature, const FLevelData&);

UCLASS()
class CISR_API UC_GameFunctionLibrary : public UBlueprintFunctionLibrary
{
    GENERATED_BODY()
};
