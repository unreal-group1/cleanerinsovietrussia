// ForgottenGods

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "C_CISRCharacter.generated.h"

UCLASS()
class CISR_API AC_CISRCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	AC_CISRCharacter();

	/** First person camera */
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
    class UCameraComponent* FirstPersonCameraComponent;

    /** MappingContext */
    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
    class UInputMappingContext* DefaultMappingContext;

protected:
	virtual void BeginPlay() override;

public:	
	virtual void Tick(float DeltaTime) override;

	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
};
